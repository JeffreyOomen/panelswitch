package Main;

import Controller.ReservationController;
import Presentation.ReservationGUI;

/**
 * Created by Matthijsske on 9-6-2015.
 */
public class Main {
    public static void main(String args[]) {
        new ReservationGUI(new ReservationController());
    }
}
