package Entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Matthijsske on 9-6-2015.
 */
public class ReservationList {
    List<Reservation> reservationList;

    public ReservationList() {
        reservationList = new ArrayList<Reservation>();
    }

    public void addReservation(Reservation reservation) {
        reservationList.add(reservation);
    }

    public int getReservationAmount() {
        return reservationList.size();
    }

    public Reservation getReservationById(int index) {
        return reservationList.get(index);
    }
}
