/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Entity;

/**
 *
 * @author Maurice
 */
public class Customer {
    private final int ID;
    private String name;

    public Customer(int ID, String name) {
        this.ID = ID;
        this.name = name;
    }

    public String getCustomerName() {
        return name;
    }


    public int getCustomerId() {
        return ID ;
    }

}
