package Entity;

import java.time.*;

/**
 * Created by Matthijsske on 9-6-2015.
 */
public class Reservation {
    private int customerId;
    private int articleId;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private int reservationNumber;

    public Reservation(int reservationNumber) {
        this.reservationNumber = reservationNumber;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public void setArticleId(int articleId) {
        this.articleId = articleId;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "Klant: "+customerId+"\n" +
                "Artikel: "+articleId+"\n" +
                "Reservering: "+reservationNumber+"\n" +
                "Start Datum: "+startDate.toString()+"\n" +
                "Eind Datum: "+endDate.toString()+"\n";

    }
}
